local addon, dark_addon = ...

local function combat()
    -- combat
end

local function resting()
    -- -- resting
end

dark_addon.rotation.register({
  --class = dark_addon.rotation.classes.priest,
  name = 'priest_buddy',
  label = 'Bundled Priest Buddy',
  combat = combat,
  resting = resting
})
